import React, { Component } from 'react';
import { View, Text } from 'react-native';
//import { createStackNavigator, createAppContainer } from 'react-navigation';
import AppNavigator from './AppNavigator';

export default class App extends Component {
  render() {
    return (
      <AppNavigator />
    )
  }
}

/*const AppContainer = createStackNavigator({
  Home: {
    screen: HomeScreen
  }
});

export default createAppContainer(AppContainer);*/