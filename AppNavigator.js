import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './HomeScreen';
import Detail from './Detail';

const AppNavigator = createStackNavigator({
  Home: HomeScreen,
  Detail: Detail,
});

export default createAppContainer(AppNavigator);