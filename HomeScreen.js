import React, { Component } from 'react';
import { View, Text, Button, TouchableOpacity } from 'react-native';
//import { createStackNavigator, createAppContainer } from 'react-navigation';


export default class HomeScreen extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
        <Text>Home Screen</Text>
        <Button title="Click me" onPress={() => {navigate('Detail'),{name: 'Tets'}}} />
      </View>
    )
  }
}

/*const AppContainer = createStackNavigator({
  Home: {
    screen: HomeScreen
  }
});

export default createAppContainer(AppContainer);*/